<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // route constains using regex yeta garesi 
        // chai aru routes haru ma implement garirakhnu pardaina
        Route::pattern('id','[0-9]+');
        Route::pattern('name','[a-z]+');

    }
}
