<?php

use App\Http\Controllers\frontend\frontendController;
use App\Http\Controllers\system\systemController;
use App\Http\Controllers\system\nested\systemNestedController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::resource('/test','App\Http\Controllers\TestController');
Route::resource('/test','TestController');

// route constrains with string using regex
Route::get('/test/{name}', function () {
    return view('welcome');
})->where('name','[a-z]+');

//  route constains with string and integer using regex 
Route::get('/test/{name}/{id}', function () {
        return view('welcome');
    })->where(['name'=>'[a-z]+','id'=>'[0-9]+']);

// Validation on appService provider
Route::get('/test/{name}/{id}', function () {
    return view('welcome');
});

// optional parameter routing
Route::get('/test/{name?}', function () {
    return view('welcome');
})->name('test');

// without group 
Route::resource('/system','system\systemController');
Route::resource('/systemlogin','system\systemLoginController');

// using group
Route::group(['namespace'=>'frontend','middleware'=>'auth'],function(){
    Route::resource('/frontend','frontendController');
    Route::resource('/frontendlogin','frontendLoginController');
});

// to auth and a nested routing using group
Route::group(['namespace'=>'system',],function(){
    Route::resource('/system','systemController');
    Route::resource('/systemlogin','systemLoginController');
    Route::group(['namespace'=>'nested'],function(){
        Route::resource('/nested','systemNestedController');
    });
});

// CRUD 
//  to get a id 
// Route::get('/test/1',[TestController::class,'show']);

// // create
// Route::get('/test/create',[TestController::class,'create']);
// Route::post('/test',[TestController::class,'store']);
// // update
// Route::get('/test/edit/1',[TestController::class,'edit']);
// Route::put('/test/1',[TestController::class,'update']);
// // delete
// Route::delete('/test/1',[TestController::class,'destroy']);



// qua_1:

Route::group([],function($route){
    // $route->resource('/user','UserController');
    $route->get('/user/{name}',[UserController::class,'show']);
});


Route::group(['middleware'=>'user'],function($route){    //logic in middleware/user.php
    // $route->resource('/user','UserController');
    $route->get('/user/{name}',[UserController::class,'show']);
});

Route::group(['prefix'=>'product'],function(){
    Route::resource('/category','categoryController');
});